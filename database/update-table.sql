-- 12 Januari 2022 (Udin)

DROP TABLE IF EXISTS `d_inventory_hop`;
CREATE TABLE IF NOT EXISTS `d_inventory_hop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lokasi_asal` int(11) DEFAULT NULL,
  `id_lokasi_tujuan` int(11) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'active',
  `distance` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;


-- 13 Januari (Ve)
ALTER TABLE `m_location` ADD `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`, ADD `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`;

ALTER TABLE `m_location` CHANGE `status` `status` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'active';

-- 14 Januari 2022 (Udin)

DROP TABLE IF EXISTS `d_hop_core`;
CREATE TABLE IF NOT EXISTS `d_hop_core` (
  `id` int(11) NOT NULL,
  `id_hop` int(11) NOT NULL,
  `id_core` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `hasil_ukur` int(11) DEFAULT NULL,
  `evidance` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'latest',
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `d_hop_core_history`;
CREATE TABLE IF NOT EXISTS `d_hop_core_history` (
  `id` int(11) NOT NULL,
  `id_hop` int(11) NOT NULL,
  `id_core` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `hasil_ukur` int(11) DEFAULT NULL,
  `evidance` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'latest',
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `d_inventory_hop`;
CREATE TABLE IF NOT EXISTS `d_inventory_hop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_lokasi_asal` int(11) DEFAULT NULL,
  `id_lokasi_tujuan` int(11) DEFAULT NULL,
  `jenis_fo` varchar(255) DEFAULT NULL,
  `jenis_kabel` varchar(255) DEFAULT NULL,
  `distance` int(11) NOT NULL DEFAULT 1 COMMENT 'distance = panjang fo',
  `no_ruas` varchar(255) DEFAULT NULL,
  `pemakaian` varchar(255) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'active',
  `qrcode` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `d_inventory_location`;

DROP TABLE IF EXISTS `m_core`;
CREATE TABLE IF NOT EXISTS `m_core` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

INSERT INTO `m_core` (`id`, `nama`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'BBone', 'active', '2022-01-12 21:22:49', '2022-01-12 21:22:49'),
	(2, 'RMJ\r\n', 'active', '2022-01-12 21:27:07', '2022-01-12 21:27:07'),
	(3, 'Metro', 'active', '2022-01-12 21:27:20', '2022-01-12 21:27:20'),
	(4, 'Lstmile', 'active', '2022-01-12 21:27:36', '2022-01-12 21:27:36'),
	(5, 'Akses', 'active', '2022-01-12 21:27:42', '2022-01-12 21:27:42'),
	(6, 'OLO', 'active', '2022-01-12 21:27:46', '2022-01-12 21:27:46'),
	(7, 'RFTS', 'active', '2022-01-12 21:27:51', '2022-01-12 21:27:51'),
	(8, 'Mara', 'active', '2022-01-12 21:27:57', '2022-01-12 21:27:57'),
	(9, 'Idle Rusak', 'active', '2022-01-12 21:28:01', '2022-01-12 21:28:01'),
	(10, 'Idle Baik', 'active', '2022-01-12 21:28:06', '2022-01-12 21:28:06'),
	(11, 'TSel', 'active', '2022-01-12 21:28:10', '2022-01-12 21:28:10'),
	(12, 'NTS', 'active', '2022-01-12 21:28:15', '2022-01-12 21:28:15');


-- 18 Januari (ve) 

  CREATE TABLE `cmon`.`d_hop_support` ( `id` INT NOT NULL AUTO_INCREMENT , 
  `id_hop` INT NULL , `lat_asal` VARCHAR(20) NULL , `lon_asal` VARCHAR(20) NULL , 
  `lat_tujuan` VARCHAR(10) NULL , `lon_tujuan` VARCHAR(20) NULL , `tipe` VARCHAR(20) NULL , 
  `keterangan` TEXT NULL , `created_at` TIMESTAMP NULL , `updated_at` TIMESTAMP NULL , 
  PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `d_hop_support` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `d_hop_support` CHANGE `updated_at` `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;


-- 19 Januari (ve)
ALTER TABLE `d_inventory_hop` ADD `tipe` VARCHAR(50) NULL AFTER `qrcode`;

ALTER TABLE `d_hop_support` DROP `lat_asal`, DROP `lon_asal`, DROP `lat_tujuan`, DROP `lon_tujuan`;

ALTER TABLE `d_hop_support` ADD `foto` TEXT NULL ;

ALTER TABLE `d_hop_support` ADD `status` VARCHAR(20) NULL AFTER `keterangan`;

ALTER TABLE `d_hop_support` ADD `lon` VARCHAR(50) NULL AFTER `tipe`, ADD `lat` VARCHAR(50) NULL AFTER `lon`;

-- 24 Januari (ve)
CREATE TABLE `cmon`.`d_laporan_patroli` ( `id` INT NOT NULL AUTO_INCREMENT , `id_hop` INT NULL , 
`pelapor` INT NULL , `lat` VARCHAR(50) NULL , `lon` VARCHAR(50) NULL , `foto` TEXT NULL , `keterangan` TEXT NULL , 
`status` VARCHAR(20) NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , 
PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 7 Februari (ve)
ALTER TABLE `d_hop_support` ADD `updated_by` INT NULL DEFAULT NULL AFTER `status`;

-- 8 Februari (ve)
ALTER TABLE `d_laporan_patroli` ADD `eskalasi` ENUM('ya', 'tidak') NULL AFTER `keterangan`;

ALTER TABLE `d_laporan_patroli` CHANGE `eskalasi` `eskalasi` ENUM('Ya','Tidak') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;

ALTER TABLE `d_hop_support` ADD `tipe_laporan` VARCHAR(15) NULL DEFAULT NULL AFTER `status`;