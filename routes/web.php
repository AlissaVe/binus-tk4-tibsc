<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/blank', function () {
    return view('blank');
});

// Start route auth
Route::get('/login', function () {
    return view('auth.login');
})->name('login');
Route::post('/login/cek', 'AuthController@loginCek')->name('login.cek');
Route::get('/register', 'AuthController@register')->name('register');
Route::get('/register/get-sto', 'AuthController@getSto')->name('register.get_sto');
Route::post('/register/simpan', 'AuthController@simpan')->name('register.simpan');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::get('/lupa-password', function () {
    return view('auth.lupa-password');
})->name('lupa_password');
Route::post('/kirim-email', 'AuthController@kirimEmail')->name('kirim_email');
Route::get('/reset-password/{id}', 'AuthController@resetPassword')->name('reset_password');
Route::post('/simpan-password', 'AuthController@simpanPassword')->name('simpan_password');
// End route auth



Route::get('/all-dimensi', 'DimensiController@index')->name('dimensi.index');
Route::get('/tambah-dimensi', 'DimensiController@tambah')->name('dimensi.tambah');
Route::get('/hapus-dimensi/{id}', 'DimensiController@hapus')->name('dimensi.hapus');
Route::post('/add-dimensi', 'DimensiController@tambah2')->name('dimensi.add');

Route::get('/all-pertanyaan', 'PertanyaanController@index')->name('pertanyaan.index');
Route::get('/tambah-pertanyaan', 'PertanyaanController@tambah')->name('pertanyaan.tambah');
Route::get('/hapus-pertanyaan/{id}', 'PertanyaanController@hapus')->name('pertanyaan.hapus');
Route::post('/add-pertanyaan', 'PertanyaanController@tambah2')->name('pertanyaan.add');

Route::get('/all-pengguna', 'PenggunaController@index')->name('pengguna.index');
Route::get('/tambah-pengguna', 'PenggunaController@tambah')->name('pengguna.tambah');
Route::get('/hapus-pengguna/{id}', 'PenggunaController@hapus')->name('pengguna.hapus');
Route::post('/add-pengguna', 'PenggunaController@tambah2')->name('pengguna.add');



// Route::get('/all-role', 'allRoleController@index')->name('allRole.index');
// Route::get('/jadwal', 'jadwalController@index')->name('jadwal.index');
// Route::get('/kelas', 'kelasController@index')->name('kelas.index');
// Route::get('/absen', 'absensiController@index')->name('absen.index');
// Route::get('/rekap', 'absensiController@rekap')->name('absen.rekap');

// Route::post('/absen/simpan', 'absensiController@simpan')->name('absen.simpan');

// Route::group(['middleware' => ['auth']], function () {

// });
