@extends('layouts.main')

@section('extra-style')
@endsection

@section('content')
    <div class="page-content">
        <div class="page-title-box">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title mb-1">Profil</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="float-right d-none d-md-block">
                            <div class="dropdown">
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <form class="form-profil">
                                    @csrf
                                    <div class="form-group mb-4">
                                        <label>Role</label>
                                        @if ($data->role == "admin_regional")
                                            <input type="text" class="form-control role" name="role" value="Admin Regional" readonly>
                                        @elseif ($data->role == "admin_arnet")
                                            <input type="text" class="form-control role" name="role" value="Admin Regional" readonly>
                                        @elseif ($data->role == "squat")
                                            <input type="text" class="form-control role" name="role" value="Squat" readonly>
                                        @endif
                                        {{-- <select name="role" class="select2 role" style="width: 100%;" onchange="getArnet()">
                                            <option value="-" disabled>- Pilih Role -</option>
                                            <option value="admin_arnet" @if ($data->role == 'admin_arnet') selected @endif>Admin Arnet</option>
                                            <option value="squat" @if ($data->role == 'squat') selected @endif>Squat</option>
                                        </select> --}}
                                    </div>
                                    <div class="form-group mb-4 container-arnet">
                                        <label>Loker (Arnet)</label>
                                        <select class="select2 arnet" multiple="multiple" name="arnet[]" style="width: 100%;">
                                            @php
                                                $tempId = [];
                                                
                                                function isSame($tempId, $id)
                                                {
                                                    for ($i = 0; $i < count($tempId); $i++) {
                                                        if ($tempId[$i] == $id) {
                                                            return true;
                                                        }
                                                    }
                                                
                                                    return false;
                                                }
                                                
                                                foreach ($arnet as $key => $ar) {
                                                    foreach ($data->loker as $key2 => $lk) {
                                                        if ($lk->id == $ar->id) {
                                                            array_push($tempId, $lk->id);
                                                        }
                                                    }
                                                }
                                            @endphp

                                            @foreach ($arnet as $key => $ar)
                                                @php
                                                    $res = isSame($tempId, $ar->id);
                                                @endphp
                                                @if ($res == true)
                                                    <option selected value="{{ $ar->id }}">{{ $ar->fullname }}
                                                    </option>
                                                @else
                                                    <option value="{{ $ar->id }}">{{ $ar->fullname }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>NIK</label>
                                        <input type="text" class="form-control nik" name="nik"
                                            value="{{ $data->nik }}" readonly>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Nama</label>
                                        <input type="text" class="form-control nama" name="nama"
                                            value="{{ $data->nama }}">
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Email</label>
                                        <input type="text" class="form-control email" name="email"
                                            value="{{ $data->email }}" readonly>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>No HP</label>
                                        <input type="text" class="form-control nohp" name="nohp"
                                            value="{{ $data->no_hp }}">
                                    </div><br>
                                    <hr>
                                    <p>*) Isi jika ingin mengganti password</p>
                                    <div class="form-group mb-4">
                                        <label for="password">New Password</label>
                                        <input type="password" class="form-control password" name="password"
                                            autocomplete="off">
                                    </div>
                                    <div class="form-group mb-4">
                                        <label for="password2">Re-type New Password</label>
                                        <input type="password" class="form-control password2" name="password2"
                                            autocomplete="off">
                                    </div>
                                </form>
                                <div class="form-group mb-4">
                                    <button type="button" class="btn btn-danger btn-sm float-right waves-effect waves-light"
                                        onclick="update()">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-script')
    <script>
        $(document).ready(function() {

        });

        function getArnet() {
            if ($(".role").val() == "admin_arnet") {
                $(".arnet").val("-").trigger("change");
                $(".container-arnet").removeClass("d-none");
            } else {
                $(".container-arnet").addClass("d-none");
            }
        }

        function update() {
            if ($('.role').val() == '-' || $('.role').val() == null || $('.role').val() == '') {
                Swal.fire({
                    title: "Peringatan",
                    text: "Role harus dipilih",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.arnet').val() == '' || $('.arnet').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Loker (Arnet) harus dipilih",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.nama').val() == '' || $('.nama').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Nama harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.nohp').val() == '' || $('.nohp').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "No HP harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.password2').val() == null && $('.password').val() != null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Re-type new password harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.password2').val() != null && $('.password2').val() != $('.password').val()) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Password tidak sama",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else {
                $.ajax({
                    url: '{{ route('profil.update') }}',
                    type: 'post',
                    data: $('.form-profil').serialize(),
                    success: function(response) {
                        if (response.status == 'berhasil') {
                            Swal.fire({
                                title: "Sukses",
                                text: "Data berhasil disimpan",
                                icon: "success",
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.reload();
                            });

                        } else if (response.status == 'gagal') {
                            Swal.fire({
                                title: "Gagal",
                                text: "Data gagal disimpan",
                                icon: "error",
                                showConfirmButton: true
                            });
                            console.log(response.message);
                        }
                    },
                    error: function(request, status, error) {
                        window.location.href = "{{ url('/login') }}";
                        console.log(request.responseText);
                    }
                });
            }
        }
    </script>
@endsection
