@extends('layouts-auth.main')

@section('extra-style-auth')
@endsection

@section('content-auth')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-sm-8">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="p-2">
                            <h5 class="mb-5 text-center">Reset Password</h5>
                            <form class="form-horizontal frm-reset-password">
                                @csrf
                                <input name="id_user" id="id-user" type="hidden" value="{{ $id }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mb-4">
                                            <label for="password">New Password</label>
                                            <input type="password" class="form-control password" name="password"
                                                autocomplete="off">
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="password2">Re-type New Password</label>
                                            <input type="password" class="form-control password2" name="password2"
                                                autocomplete="off">
                                        </div>
                                        <div class="mt-4">
                                            <button class="btn btn-success btn-block waves-effect waves-light btn-reset" type="button" onclick="simpanPassword()">Reset Password</button>
                                            <div class="btn btn-secondary ld-ext-top running d-none btn-reset-loading" style="width: 100%;">
                                                Loading
                                                <div class="ld ld-ball ld-flip-h"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-script-auth')
    <script type="text/javascript">
        $(document).ready(function() {

        });

        function simpanPassword() {
            if ($(".password").val() == "" || $(".password").val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Password harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($(".password2").val() == null && $(".password").val() != null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Re-type new password harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($(".password2").val() != null && $(".password2").val() != $(".password").val()) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Password tidak sama",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else {
                $.ajax({
                    url: "{{ route('simpan_password') }}",
                    type: "post",
                    data: $(".frm-reset-password").serialize(),
                    success: function(response) {
                        if (response.status == "berhasil") {
                            Swal.fire({
                                title: "Sukses",
                                text: "Password berhasil direset, silahkan login",
                                icon: "success",
                                showConfirmButton: true
                            }).then(function() {
                                window.location.href = "{{ route('login') }}";
                            });

                        } else if (response.status == 'gagal') {
                            Swal.fire({
                                title: "Gagal",
                                text: "Password gagal direset",
                                icon: "error",
                                showConfirmButton: true
                            });
                            console.log(response.message);
                        }
                    },
                    error: function(request, status, error) {
                        swal({
                            type: "error",
                            title: "Error",
                            text: "Terjadi kesalahan",
                            showConfirmButton: true
                        });
                        console.log(request.responseText);
                    }
                });
            }
        }
    </script>
@endsection
