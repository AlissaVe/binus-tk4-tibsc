@extends('layouts-auth.main')

@section('extra-style-auth')
@endsection

@section('content-auth')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-sm-8">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="p-2">
                            <h5 class="mb-5 text-center">Lupa Password</h5>
                            <form class="form-horizontal frm-lupa-password">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mb-4">
                                            <label for="nik">NIK</label>
                                            <input type="nik" class="form-control nik" name="nik" required>
                                        </div>
                                        <div class="mt-4">
                                            <button class="btn btn-success btn-block waves-effect waves-light btn-kirim" type="button" onclick="kirimEmail()">Kirim Email Reset Password</button>
                                            <div class="btn btn-secondary ld-ext-top running d-none btn-kirim-loading" style="width: 100%;">
                                                Loading
                                                <div class="ld ld-ball ld-flip-h"></div>
                                            </div>
                                        </div>
                                        <div class="mt-4 text-center">
                                            <a href="{{ route('login') }}" class="text-muted"><i class="mdi mdi-account-circle mr-1"></i> Sudah punya akun ?</a>
                                        </div>
                                        <div class="mt-4 text-center">
                                            <a href="{{ route('register') }}" class="text-muted"><i class="mdi mdi-account-circle mr-1"></i> Buat akun</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-script-auth')
    <script type="text/javascript">
        $(document).ready(function() {
            
        });

        function kirimEmail() {
            if ($(".nik").val() == "" || $(".nik").val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "NIK harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else {
                $('.btn-kirim').addClass('d-none');
                $('.btn-kirim-loading').removeClass('d-none');
                let email = $(".email").val();

                $.ajax({
                    url: "{{ route('kirim_email') }}",
                    type: "post",
                    data: $(".frm-lupa-password").serialize(),
                    success: function(response) {
                        $('.btn-kirim-loading').addClass('d-none');
                        $('.btn-kirim').removeClass('d-none');
                        if (response.status == "ada") {
                            Swal.fire({
                                title: "Sukses",
                                text: "Link reset password telah berhasil dikirim, silahkan cek email anda",
                                icon: "success",
                                showConfirmButton: true
                            });

                        } else if (response.status == "kosong") {
                            Swal.fire({
                                title: "Peringatan",
                                text: "NIK anda tidak terdaftar",
                                icon: "warning",
                                showConfirmButton: true
                            });

                        } else if (response.status == "gagal") {
                            Swal.fire({
                                title: "Gagal",
                                text: "Email reset password gagal dikirim",
                                icon: "error",
                                showConfirmButton: true
                            });
                            console.log(response.message);
                        }
                    },
                    error: function(request, status, error) {
                        $('.btn-kirim-loading').addClass('d-none');
                        $('.btn-kirim').removeClass('d-none');
                        Swal.fire({
                            title: "Error",
                            text: "Terjadi kesalahan",
                            icon: "error",
                            showConfirmButton: true
                        });
                        console.log(request.responseText);
                    }
                });
            }
        }
    </script>
@endsection
