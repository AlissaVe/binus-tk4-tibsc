@extends('layouts-auth.main')

@section('extra-style-auth')
    <style>
        .modal-dialog {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }

        .modal-content {
            height: auto;
            min-height: 100%;
            border-radius: 0;
        }

        #screenshot-ktp>img,
        #screenshot-selfie>img,
        #screenshot-selfie-ktp>img {
            width: 100%;
            height: auto;
        }

    </style>
@endsection

@section('content-auth')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-sm-8">
                <div class="card">
                    <div class="card-body p-4">
                        <div class="p-2">
                            <h5 class="mb-5 text-center">Register</h5>
                            <form class="form-horizontal form-register">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group mb-4">
                                            <label for="nama">Role</label>
                                            <select class="select2 w-100 role" name="role">
                                                <option value="-" disabled="" selected="" disabled>- Pilih Role -</option>
                                                <option value="organik">Organik Telkom</option>
                                                <option value="non_organik">Non Organik Telkom</option>
                                            </select>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="nama">Arnet</label>
                                            <select class="select2 arnet" name="arnet[]" multiple="multiple" style="width: 100%;">
                                                @foreach ($arnet as $ar)
                                                    <option value="{{ $ar->id }}">{{ $ar->fullname }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="nama">NIK</label>
                                            <input type="number" class="form-control nik" name="nik" required>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="nama">Nama</label>
                                            <input type="text" class="form-control nama" name="nama" required>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control email" name="email" required>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="no_hp">Nomer HP</label>
                                            <input type="number" class="form-control no-hp" name="no_hp" required>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control password" name="password" required>
                                        </div>
                                        <div class="form-group mb-4">
                                            <label for="password2">Re-type Password</label>
                                            <input type="password" class="form-control password2" name="password2" required>
                                        </div>
                                        <div class="mt-4">
                                            <button class="btn btn-primary btn-block waves-effect waves-light btn-register"
                                                type="button" onclick="simpan()">Register</button>
                                            <div class="btn btn-secondary ld-ext-top running d-none btn-register-loading"
                                                style="width: 100%;">
                                                Loading
                                                <div class="ld ld-ball ld-flip-h"></div>
                                            </div>
                                        </div>
                                        <div class="mt-4 text-center">
                                            <a href="{{ route('login') }}" class="text-muted"><i
                                                    class="mdi mdi-account-circle mr-1"></i> Sudah punya akun ?</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('auth.modal')
@endsection

@section('extra-script-auth')
    <script type="text/javascript">
        function getArnet() {
            if ($(".role").val() == "admin_arnet") {
                $(".arnet").val("-").trigger("change");
                $(".container-arnet").removeClass("d-none");
            } else {
                $(".container-arnet").addClass("d-none");
            }
        }

        function simpan() {
            if ($('.role').val() == '-' || $('.role').val() == null || $('.role').val() == '') {
                Swal.fire({
                    title: "Peringatan",
                    text: "Role harus dipilih",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.arnet').val() == '' || $('.arnet').val() == '-' || $('.arnet').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Arnet harus dipilih",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.nik').val() == '' || $('.nik').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "NIK harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.nama').val() == '' || $('.nama').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Nama harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.email').val() == '' || $('.email').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Email harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.no-hp').val() == '' || $('.no-hp').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "No HP harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.password').val() == '' || $('.password').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Password harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.password2').val() == '' || $('.password').val() == null) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Re-type Password harus diisi",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else if ($('.password2').val() != $('.password').val()) {
                Swal.fire({
                    title: "Peringatan",
                    text: "Password tidak sama",
                    icon: "warning",
                    showConfirmButton: true
                });
            } else {
                $('.btn-register').addClass('d-none');
                $('.btn-register-loading').removeClass('d-none');

                $.ajax({
                    url: '{{ route('register.simpan') }}',
                    type: 'post',
                    data: $('.form-register').serialize(),
                    success: function(response) {
                        $('.btn-register-loading').addClass('d-none');
                        $('.btn-register').removeClass('d-none');
                        if (response.status == 'berhasil') {
                            Swal.fire({
                                title: "Sukses",
                                text: "Data berhasil disimpan, silahkan login",
                                icon: "success",
                                showConfirmButton: false,
                                timer: 1000
                            }).then(function() {
                                window.location.href = "{{ route('login') }}";
                            });

                        } else if (response.status == 'ada') {
                            Swal.fire({
                                title: "Peringatan",
                                text: response.message,
                                icon: "warning",
                                showConfirmButton: true
                            });

                        } else if (response.status == 'gagal') {
                            Swal.fire({
                                title: "Gagal",
                                text: "Data gagal disimpan",
                                icon: "error",
                                showConfirmButton: true
                            });
                            console.log(response.message);
                        }
                    },
                    error: function(request, status, error) {
                        $('.btn-register-loading').addClass('d-none');
                        $('.btn-register').removeClass('d-none');
                        Swal.fire({
                            title: "Error",
                            text: "Terjadi kesalahan, hubungi pengembang",
                            icon: "error",
                            showConfirmButton: true
                        });
                        console.log(request.responseText);
                    }
                });
            }
        }
    </script>
@endsection
