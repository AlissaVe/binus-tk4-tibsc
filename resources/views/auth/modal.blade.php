<div class="modal fade modal-webcam" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Ambil Foto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="modal-type">
                <div class="text-center">
                    <video id="video" autoplay playsinline style="width: 100%; height: auto;"></video>
                </div>
                <button class="button d-none" id="btn-play">
                    <span class="icon is-small">
                        <i class="fas fa-play"></i>
                    </span>
                </button>
                <div class="text-center">
                    {{-- <button type="button" class="btn btn-secondary float-left" id="btn-change-camera"
                        onclick="changeCamera()" style="width: 40%;">Flip
                        Camera</button> --}}
                    <button type="button" class="btn btn-primary float-right" id="btn-screenshot" onclick="screenshot()"
                        style="width: 100%;">Snap</button>
                </div>
            </div>
        </div>
    </div>
</div>
