<!-- JAVASCRIPT -->
<script src="{{ asset('template/libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('template/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('template/libs/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('template/libs/node-waves/waves.min.js') }}"></script>

<script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>
<!-- Required datatable js -->
<script src="{{ asset('template/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('template/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('template/libs/select2/select2.min.js') }}"></script>
<!-- Sweet Alerts js -->
<script src="{{ asset('template/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Moment js -->
<script src="{{ asset('template/libs/moment/moment.js') }}"></script>
<script src="{{ asset('template/libs/moment/locale/id.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('template/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('template/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
<!-- Datatable init js -->
<script src="{{ asset('template/js/pages/datatables.init.js') }}"></script>
<!-- Datatable fixed column -->
<script src="{{ asset('template/libs/datatables.net-fixedcolumn/js/dataTables.fixedColumn.min.js') }}"></script>
<!-- Datatable search pane -->
<script src="{{ asset('template/libs/datatables.net-searchpanes/js/dataTables.searchpanes.min.js') }}"></script>
<!-- Datatable select -->
<script src="{{ asset('template/libs/datatables.net-select/js/dataTables.select133.min.js') }}"></script>

<script src="{{ asset('template/js/app.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
