<!doctype html>
<html lang="id">

<head>
    @include('layouts-auth.partials.head')
    @include('layouts-auth.partials.style')
    @yield('extra-style-auth')
</head>

<body class="bg-primary bg-pattern">
    <div class="account-pages py-3 pt-sm-5">
        @yield('content-auth')
    </div>

    @include('layouts-auth.partials.script')
    @yield('extra-script-auth')
</body>

</html>
