@extends('layouts.main')
@section('content')
@include('layouts.alert')

<div class="box">

	<div class="box-header" data-original-title>
		<h2><i class="halflings-icon white edit"></i><span class="break"></span>Tambah Data Dimensi</h2>
		<div class="box-icon">
			<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
			<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
			<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
		</div>
	</div>
	<div class="box-content">
	<form class="form-horizontal" method="POST" action="{{url('add-pengguna')}}" >
        @csrf
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Username : </label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="focusedInput" type="text" value="" name="username">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Password : </label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="password" value="" name="password">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="focusedInput">Hak Akses : </label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" value="" name="hak_akses">
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" name="simpan">Save changes</button>
                    <button class="btn">Cancel</button>
                </div>
            </fieldset>
        </form>
		<?php
		if (isset($_POST['simpan'])) {
			$dimensi = $_POST['dimensi'];
			$bobot = $_POST['bobot'];

			$mysqli->query("insert into tbdimensi (dimensi,bobot) values ('$dimensi',$bobot)");
			header('location:tambahdimensi.php');
		}
		?>
	</div>
</div><!--/span-->

@endsection

@section('extra-script')

@endsection