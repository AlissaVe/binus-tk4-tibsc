@extends('layouts.main')



@section('content')
<div class="row-fluid sortable">
    @include('layouts.alert')
    <div class="box">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Pengguna</h2>

            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>

        </div>

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">

                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Hak Akses</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $dt)
                    <tr>
                        <td>{{$dt->user_id}}</td>
                        <td>{{$dt->username}}</td>
                        <td>{{$dt->password}}</td>
                        <td>{{$dt->hak_akses}}</td>
                        <td class="center">
                            <a class="btn btn-danger" onclick="return confirm('Apakah anda yakin menghapus pengguna tersebut?')" href="{{ route('pengguna.hapus', $dt->user_id) }}">
                                <i class="halflings-icon white trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div><!--/span-->

</div><!--/row-->
</div><!--/row-->
@endsection

@section('extra-script')

@endsection