@extends('layouts.main')

@section('content')
<div class="row-fluid sortable">
@include('layouts.alert')
    <div class="box">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Dimensi</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">

                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Dimensi</th>
                        <th>Bobot</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $dt)
                    <tr>
                        <td>{{$dt->id_dimensi}}</td>
                        <td>{{$dt->dimensi}}</td>
                        <td>{{$dt->bobot}}</td>
                        <td class="center">
                            <a class="btn btn-success" href="#">
                                <i class="halflings-icon white zoom-in"></i>
                            </a>
                            <a class="btn btn-danger" onclick="return confirm('Apakah anda yakin menghapus dimensi tersebut?')" href="{{ route('dimensi.hapus', $dt->id_dimensi) }}">
                                <i class="halflings-icon white trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div><!--/span-->

</div><!--/row-->
</div><!--/row-->
@endsection

@section('extra-script')

@endsection