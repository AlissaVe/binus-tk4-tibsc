@extends('layouts.main')
@section('content')
@include('layouts.alert')

<div class="box">

    <div class="box-header" data-original-title>

        <h2><i class="halflings-icon white edit"></i><span class="break"></span>Tambah Data Dimensi</h2>
        <div class="box-icon">
            <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
            <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
            <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
        </div>
    </div>
    <div class="box-content">
        <form class="form-horizontal dimensi" method="POST" action="{{url('add-dimensi')}}">
            @csrf 
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Dimensi : </label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="focusedInput" type="text" value="" name="dimensi">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="focusedInput">Bobot : </label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="" type="text" value="" name="bobot">
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" onclick="simpan()" name="simpan">Save changes</button>
                    <button class="btn">Cancel</button>
                </div>
            </fieldset>
        </form>
       
    </div>
</div><!--/span-->

@endsection

@section('extra-script')

<script>
function simpan(){
    alert(1234);
}
</script>
@endsection