<!doctype html>
<html lang="id">

<head>
	@include('layouts.partials.head')


</head>

<body>

	<!-- start: Header -->
	@include('layouts.partials.navbar')


	<div class="container-fluid-full">
		<div class="row-fluid">

			@include('layouts.partials.sidebar')

			<!-- start: Content -->
			<div id="content" class="span10">


				<ul class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="template.php">Home</a>
						<i class="icon-angle-right"></i>
					</li>
					<li><a href="#">Dashboard</a></li>
				</ul>

				@yield('content')
			

			</div><!--/#content.span10-->
		</div><!--/fluid-row-->

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2013 <a href="http://themifycloud.com/downloads/janux-free-responsive-admin-dashboard-template/" alt="Bootstrap_Metro_Dashboard">JANUX Responsive Dashboard</a></span>

		</p>

	</footer>

	<!-- start: JavaScript-->
	<script src="{{ asset('template/js/jquery-1.9.1.min.js') }}"></script>
	<script src="{{ asset('template/js/jquery-migrate-1.0.0.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery-ui-1.10.0.custom.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.ui.touch-punch.js') }}"></script>

	<script src="{{ asset('template/js/modernizr.js') }}"></script>

	<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.cookie.js') }}"></script>

	<script src="{{ asset('template/js/fullcalendar.min.js')}}"></script>

	<script src="{{ asset('template/js/jquery.dataTables.min.js')}}"></script>

	<script src="{{ asset('template/js/excanvas.js') }}"></script>
	<script src="{{ asset('template/js/jquery.flot.js') }}"></script>
	<script src="{{ asset('template/js/jquery.flot.pie.js') }}"></script>
	<script src="{{ asset('template/js/jquery.flot.stack.js') }}"></script>
	<script src="{{ asset('template/js/jquery.flot.resize.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.chosen.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.uniform.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.cleditor.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.noty.js') }}"></script>

	<script src="{{ asset('template/js/jquery.elfinder.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.raty.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.iphone.toggle.js') }}"></script>

	<script src="{{ asset('template/js/jquery.uploadify-3.1.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.gritter.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.imagesloaded.js') }}"></script>

	<script src="{{ asset('template/js/jquery.masonry.min.js') }}"></script>

	<script src="{{ asset('template/js/jquery.knob.modified.js') }}"></script>

	<script src="{{ asset('template/js/jquery.sparkline.min.js') }}"></script>

	<script src="{{ asset('template/js/counter.js') }}"></script>

	<script src="{{ asset('template/js/retina.js') }}"></script>

	<script src="{{ asset('template/js/custom.js') }}"></script>

</body>

</html>