<!DOCTYPE html>
<html class="loading" data-textdirection="ltr" lang="en">
<link rel="stylesheet" type="text/css" href="{{ asset('template/css/bootstrap.min.css') }}">
<!-- BEGIN: Body-->
<style>
    body {
        background: none !important;
    }

</style>

<body>

    @yield('content')

</body>
<!-- END: Body-->

</html>
