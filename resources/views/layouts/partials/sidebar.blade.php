<!-- start: Main Menu -->
<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="http://localhost/IT_BSC/template.php"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-envelope"></i><span class="hidden-tablet"> Dimensi</span> <span class="label label-important"> 2 </span></a>
                <ul>
                    <li><a class="submenu" href="{{ route('dimensi.index') }}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Lihat Data</span></a></li>
                    <li><a class="submenu" href="{{ route('dimensi.tambah') }}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Tambah Data</span></a></li>
                </ul>
            </li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-tasks"></i><span class="hidden-tablet"> Pertanyaan</span> <span class="label label-important"> 2 </span></a>
                <ul>
                    <li><a class="submenu" href="{{ route('pertanyaan.index') }}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Lihat Data</span></a></li>
                    <li><a class="submenu" href="{{ route('pertanyaan.tambah') }}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Tambah Data</span></a></li>
                </ul>
            </li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-folder-open"></i><span class="hidden-tablet"> Pengguna</span> <span class="label label-important"> 2 </span></a>
                <ul>
                    <li><a class="submenu" href="{{ route('pengguna.index') }}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Lihat Data</span></a></li>
                    <li><a class="submenu" href="{{ route('pengguna.tambah') }}"><i class="icon-file-alt"></i><span class="hidden-tablet"> Tambah Data</span></a></li>
                </ul>
            </li>
            <li><a href="http://localhost/IT_BSC/kuesioner.php"><i class="icon-edit"></i><span class="hidden-tablet"> Jawab Kuesioner</span></a></li>

        </ul>
    </div>
</div>
<!-- end: Main Menu -->