<script src="{{ asset('template/js/jquery-1.9.1.min.js') }}"></script>
<script src="{{ asset('template/js/jquery-migrate-1.0.0.min.js') }}"></script>

<script src="{{ asset('template/js/jquery-ui-1.10.0.custom.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.ui.touch-punch.js') }}"></script>

<script src="{{ asset('template/js/modernizr.js') }}"></script>

<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.cookie.js') }}"></script>

<script src="{{ asset('template/js/fullcalendar.min.js')}}"></script>

<script src="{{ asset('template/js/jquery.dataTables.min.js')}}"></script>

<script src="{{ asset('template/js/excanvas.js') }}"></script>
<script src="{{ asset('template/js/jquery.flot.js') }}"></script>
<script src="{{ asset('template/js/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('template/js/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('template/js/jquery.flot.resize.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.chosen.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.uniform.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.cleditor.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.noty.js') }}"></script>

<script src="{{ asset('template/js/jquery.elfinder.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.raty.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.iphone.toggle.js') }}"></script>

<script src="{{ asset('template/js/jquery.uploadify-3.1.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.gritter.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.imagesloaded.js') }}"></script>

<script src="{{ asset('template/js/jquery.masonry.min.js') }}"></script>

<script src="{{ asset('template/js/jquery.knob.modified.js') }}"></script>

<script src="{{ asset('template/js/jquery.sparkline.min.js') }}"></script>

<script src="{{ asset('template/js/counter.js') }}"></script>

<script src="{{ asset('template/js/retina.js') }}"></script>

<script src="{{ asset('template/js/custom.js') }}"></script>
