<style type="text/css">
    .d-block {
        display: block;
    }

    .d-none {
        display: none;
    }

    /*.modal-dialog {
  margin: 1.7rem;
 }*/

    .pointer {
        cursor: pointer;
    }

    .w-100 {
        width: 100%;
    }

</style>

<style>
    @media screen and (max-width: 767px) {
        li.paginate_button.previous {
            display: inline;
        }

        li.paginate_button.next {
            display: inline;
        }

        li.paginate_button {
            display: none;
        }
    }

    .select2-container--default .select2-results__option--highlighted.select2-results__option--selectable {
        background-color: #2fa97c;
        color: white;
    }

</style>
