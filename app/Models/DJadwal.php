<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DJadwal extends Model
{
    protected $table = "d_jadwal";

    public function getKelas()
    {
        return $this->hasMany('App\Models\DKelas', 'id' , 'id_kelas');
    }

    public function getGuru()
    {
        return $this->hasMany('App\User', 'id', 'id_guru');
    }
}
