<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DPertanyaan extends Model
{
    protected $table = "tbkuesioner";
    protected $primaryKey = 'id_kuesioner';
    
    public $timestamps = false;
    public function getDimensi()
    {
        return $this->hasMany('App\Models\DDimensi', 'id' , 'id_pertanyaan');
    }
}
