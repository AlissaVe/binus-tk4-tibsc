<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DDimensi extends Model
{
    protected $table = "tbdimensi";
    protected $primaryKey = 'id_dimensi';
    public $timestamps = false;
}
