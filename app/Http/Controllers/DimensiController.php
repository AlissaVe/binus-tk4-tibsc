<?php
namespace App\Http\Controllers;
use App\Models\DDimensi;
use App\User;
use Illuminate\Http\Request;

class DimensiController extends Controller
{
    public function index(Request $request)
    {
        $data = DDimensi::get();
        return view('dimensi.index', compact('data'));
    }

    public function tambah()
    {
        return view('dimensi.tambah');
    }

    public function hapus($id)
    {
        DDimensi::where('id_dimensi', $id)->delete();
        return redirect()->route('dimensi.index')->with('success', 'Dimensi Deleted successfully!');
    }

    function tambah2(Request $request){
        $dt = new DDimensi();
        $dt->dimensi = $request->dimensi;
        $dt->bobot = $request->bobot;
        $dt->save();
        return redirect()->route('dimensi.tambah')->with('success', 'Dimensi Added successfully!');

    }
}
