<?php

namespace App\Http\Controllers;

use App\Helpers\ImageProcess;
use App\Models\MPerangkatIp;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Storage;
use Telegram;
use URL;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\DHopCore;
use App\Models\DHopCoreHistory;
use App\Models\DHopSupport;
use App\Models\DInventoryHop;
use App\Models\DLaporanPatroli;
use App\Models\MCore;



class ScanQrController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return view('scan-qr.index');
    }

    public function getPerangkatIp(Request $request)
    {
        DB::beginTransaction();
        try {
            if ($request->qrcode != null) {
                $data = MPerangkatIp::where('qrcode', $request->qrcode);
            }

            $data = $data->with('getLokasiFixom')
                    ->where('status', 'active')
                    ->first();

            // dd($data);

            // $user = Auth::user()->id;

            if ($data == null) {
                return response()->json([
                    'status' => 'tidak ada'
                ]);

            } else {
                // $data2 = DHopSupport::where('id_hop', $data->id)
                //     ->where('status', 'latest')
                //     ->get();

                // $data3 = DLaporanPatroli::where('id_hop', $data->id)
                //     ->where('status', 'latest')
                //     ->get();

                DB::commit();
                return response()->json([
                    'status' => 'berhasil',
                    // 'data3' => $data3,
                    'data' => $data,
                    // 'data2' => $data2,
                    // 'user' => $user
                ]);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getHope(Request $request)
    {
        DB::beginTransaction();
        try {
            if ($request->qrcode != null && $request->idHop != 'NULL') {
                return response()->json([
                    'status' => 'keduanya diisi'
                ]);
            }

            if ($request->qrcode != null) {
                $data = DInventoryHop::where('qrcode', $request->qrcode);
            }
            
            if ($request->idHop != 'NULL') {
                $data = DInventoryHop::where('id', $request->idHop);
            }

            $data = $data->with('getLokasiAsal')
            ->with('getLokasiTujuan')
            ->with(['getHopCore' => function ($q) {
                $q->where('status', 'latest');
                // $q->orderByRaw('CAST(nama AS INT) ASC');
                $q->with('getCore');
                $q->with('getUpdatedBy');
                }])
                ->where('status', 'active')
                ->first();

            $user = Auth::user()->id;

            $core = MCore::where('status', 'active')->get();

            if ($data == null) {
                return response()->json([
                    'status' => 'tidak ada'
                ]);

            } else {
                $data2 = DHopSupport::where('id_hop', $data->id)
                    ->where('status', 'latest')
                    ->get();

                $data3 = DLaporanPatroli::where('id_hop', $data->id)
                    ->where('status', 'latest')
                    ->get();

                DB::commit();
                return response()->json([
                    'status' => 'berhasil',
                    'data3' => $data3,
                    'data' => $data,
                    'data2' => $data2,
                    'user' => $user,
                    'core' => $core
                ]);
            }

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getPatroli(Request $request)
    {


        DB::beginTransaction();
        try {


            $patroli = DLaporanPatroli::where('id_hop', $request->id)
                        ->where('status', 'latest')
                        ->with('getUser')
                        ->get();

            // dd($patroli);

            $user = Auth::user()->id;

            $core = MCore::where('status', 'active')->get();

            if ($patroli == null) {
                return response()->json([
                    'status' => 'tidak ada'
                ]);
            } else {
                DB::commit();
                return response()->json([
                    'status' => 'berhasil',
                    'patroli' => $patroli,
                    'user' => $user,
                    'core' => $core
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getHopList(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = DB::select(
                "SELECT ih.id, ih.qrcode, ih.nama_kabel, ih.jenis_kabel, la.distance, la.abbreviation as lokasi_asal_abbreviation , lt.abbreviation as lokasi_tujuan_abbreviation 
                FROM 
                    d_inventory_hop ih, 
                    (SELECT 
                        (  3959 *
                        acos(cos(radians('$request->lat')) * 
                        cos(radians(lat)) * 
                        cos(radians(lon) - 
                        radians('$request->lon')) + 
                        sin(radians('$request->lat')) * 
                        sin(radians(lat )))
                        ) AS distance, id, abbreviation 
                            FROM m_location  
                        ORDER BY `distance`  ASC) as la, 
                    m_location  lt 
                WHERE ih.id_lokasi_asal = la.id AND ih.id_lokasi_tujuan = lt.id AND la.distance < 30 and ih.status = 'active' ORDER BY la.distance LIMIT 30"
            );

            if ($data == null) {
                return response()->json([
                    'status' => 'tidak ada'
                ]);
            } else {
                DB::commit();
                return response()->json([
                    'status' => 'berhasil',
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateCore(Request $request)
    {
        DB::beginTransaction();
        try {
            // dd($request->all());
            $now = Carbon::now()->format('Ymd-His');
            
            for ($i = 0; $i < count($request->nama); $i++) {
                $hop = DInventoryHop::where('id', $request->hop)
                    ->with('getLokasiAsal')
                    ->with('getLokasiTujuan')
                    ->first();

                $ruas = $hop->getLokasiAsal->abbreviation . '_' . $hop->getLokasiTujuan->abbreviation . '(' . $hop->jenis_kabel . ')-' . $hop->qrcode . '-' . $hop->nama_kabel;
                $watermarkText = Auth::user()->nama . "\n" . Carbon::createFromFormat('Ymd-His', $now)->isoFormat('D MMMM Y (HH:mm:ss)') . "\n" . $ruas . "\n";
                $path = storage_path('app/upload/evidance/hop-core/');
                
                if ($request->status_input[$i] == "created") {
                    // dd($request->id_hop_core[$i], $request->status_input[$i]);
                    // create ke table d_hop_core
                    $hopCore = new DHopCore();
                    $hopCore->id = DHopCore::max('id') + 1;
                    $hopCore->id_hop = $request->hop;
                    $hopCore->id_core = $request->core[$i];
                    $hopCore->nama = $request->nama[$i];
                    $hopCore->customer = $request->customer[$i];
                    $hopCore->deskripsi = $request->deskripsi[$i];
                    $hopCore->hasil_ukur = $request->hasil_ukur[$i];
                    if ($request->encode_foto[$i] != null) {
                        $foto_core = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto[$i]));
                        $imageWatermarked = ImageProcess::addWatermark($foto_core, $watermarkText, 5, 57, 'string');
                        $hopCore->evidance = $now . '-' . $hopCore->id . ".png";
                        imagepng($imageWatermarked, $path . $hopCore->evidance);
                    }
                    $hopCore->tipe = $request->tipe[$i];
                    $hopCore->panjang = $request->panjang_core[$i];
                    $hopCore->status = 'latest';
                    $hopCore->updated_by = Auth::user()->id;

                    // create ke table d_hop_core_history
                    $hopCoreHistory = new DHopCoreHistory();
                    $hopCoreHistory->id = DHopCoreHistory::max('id') + 1;
                    $hopCoreHistory->id_hop = $hopCore->id_hop;
                    $hopCoreHistory->id_core = $hopCore->id_core;
                    $hopCoreHistory->nama = $hopCore->nama;
                    $hopCoreHistory->customer = $hopCore->customer;
                    $hopCoreHistory->deskripsi = $hopCore->deskripsi;
                    $hopCoreHistory->hasil_ukur = $hopCore->hasil_ukur;
                    if ($request->encode_foto[$i] != null) {
                        $hopCoreHistory->evidance = $hopCore->evidance;
                    }
                    $hopCoreHistory->tipe = $hopCore->tipe;
                    $hopCoreHistory->panjang = $hopCore->panjang;
                    $hopCoreHistory->status = 'created';
                    $hopCoreHistory->updated_by = $hopCore->updated_by;

                    // update jumlah_core in table hop
                    $hop->jumlah_core = $hop->jumlah_core + 1;

                    // save
                    $hopCoreHistory->save();
                    $hopCore->save();
                    $hop->save();
                }
                if ($request->status_input[$i] == "edited") {
                    // dd($data, $request->status_input[$i]);
                    // update ke table d_hop_core
                    $hopCore = DHopCore::where('id', $request->id_hop_core[$i])->first();
                    $hopCore->id_hop = $request->hop;
                    $hopCore->id_core = $request->core[$i];
                    $hopCore->nama = $request->nama[$i];
                    $hopCore->customer = $request->customer[$i];
                    $hopCore->deskripsi = $request->deskripsi[$i];
                    $hopCore->hasil_ukur = $request->hasil_ukur[$i];
                    if ($request->encode_foto[$i] != null) {
                        $foto_core = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto[$i]));
                        $imageWatermarked = ImageProcess::addWatermark($foto_core, $watermarkText, 5, 57, 'string');
                        $hopCore->evidance = $now . '-' . $hopCore->id . ".png";
                        imagepng($imageWatermarked, $path . $hopCore->evidance);
                    }
                    $hopCore->tipe = $request->tipe[$i];
                    $hopCore->panjang = $request->panjang_core[$i];
                    $hopCore->status = 'latest';
                    $hopCore->updated_by = Auth::user()->id;

                    // create ke table d_hop_core_history
                    $hopCoreHistory = new DHopCoreHistory();
                    $hopCoreHistory->id = DHopCoreHistory::max('id') + 1;
                    $hopCoreHistory->id_hop = $hopCore->id_hop;
                    $hopCoreHistory->id_core = $hopCore->id_core;
                    $hopCoreHistory->nama = $hopCore->nama;
                    $hopCoreHistory->customer = $hopCore->customer;
                    $hopCoreHistory->deskripsi = $hopCore->deskripsi;
                    $hopCoreHistory->hasil_ukur = $hopCore->hasil_ukur;
                    if ($request->encode_foto[$i] != null) {
                        $hopCoreHistory->evidance = $hopCore->evidance;
                    }
                    $hopCoreHistory->tipe = $hopCore->tipe;
                    $hopCoreHistory->panjang = $hopCore->panjang;
                    $hopCoreHistory->status = 'updated';
                    $hopCoreHistory->updated_by = $hopCore->updated_by;

                    // save
                    $hopCoreHistory->save();
                    $hopCore->save();
                }
                if ($request->status_input[$i] == "deleted") {
                    // dd($data, $request->status_input[$i]);
                    // ambil data dari table d_hop_core
                    $hopCore = DHopCore::where('id', $request->id_hop_core[$i])->first();
                    $hopCore->status = 'deleted';
                    $hopCore->updated_by = Auth::user()->id;

                    // create ke table d_hop_core_history
                    $hopCoreHistory = new DHopCoreHistory();
                    $hopCoreHistory->id = DHopCoreHistory::max('id') + 1;
                    $hopCoreHistory->id_hop = $hopCore->id_hop;
                    $hopCoreHistory->id_core = $hopCore->id_core;
                    $hopCoreHistory->nama = $hopCore->nama;
                    $hopCoreHistory->customer = $hopCore->customer;
                    $hopCoreHistory->deskripsi = $hopCore->deskripsi;
                    $hopCoreHistory->hasil_ukur = $hopCore->hasil_ukur;
                    if ($hopCore->evidance != null) {
                        $hopCoreHistory->evidance = $hopCore->evidance;
                    }
                    $hopCoreHistory->tipe = $hopCore->tipe;
                    $hopCoreHistory->panjang = $hopCore->panjang;
                    $hopCoreHistory->status = $hopCore->status;
                    $hopCoreHistory->updated_by = $hopCore->updated_by;

                    // update jumlah core
                    $hop->jumlah_core = $hop->jumlah_core - 1;

                    // save
                    $hopCoreHistory->save();
                    $hopCore->save();
                    $hop->save();
                }
            }

            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateTSambung(Request $request)
    {
        
        // dd($request->all());

        // dd($request->id_tsambung, $request->status_input, $request->tipe, $request->keterangan, $request->lon, $request->lat );
        DB::beginTransaction();
        try {
            // dd($request->all());
            $now = Carbon::now()->format('Ymd-His');
            
            for ($i = 0; $i < count($request->status_input); $i++) {
                // if ($request->status_input[$i] == "created") {
                //     // dd($request->encode_foto[$i]);

                //     // dd($request->id_hop_core[$i], $request->status_input[$i]);
                //     // create ke table d_hop_core
                //     $tSambung = new DHopSupport();
                //     $tSambung->id = DHopSupport::max('id') + 1;
                //     $tSambung->id_hop = $request->hop;
                //     $tSambung->tipe = $request->tipe[$i];
                //     $tSambung->lon = $request->lon[$i];
                //     $tSambung->lat = $request->lat[$i];
                //     $tSambung->keterangan = $request->keterangan[$i];
                //     if ($request->encode_foto[$i] != null) {
                //         $foto_ts = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto[$i]));
                //         $tSambung->foto = $now . '-' . $tSambung->id . ".png";
                //         Storage::disk('local')->put('/upload/titiksambung/hop/' . $tSambung->foto, $foto_ts);
                //     }

                //     $tSambung->status = 'latest';

                //     $tSambung->save();
                // }
                
                if ($request->status_input[$i] == "edited") {

                    $tSambung = DHopSupport::where('id', $request->id_tsambung[$i])->first();
                    // dd($tSambung);
                    // array_push($arredit, $tSambung);


                    $tSambung->tipe = $request->tipe[$i];
                    $tSambung->lon = $request->lon[$i];
                    $tSambung->lat = $request->lat[$i];
                    $tSambung->keterangan = $request->keterangan[$i];
                    
                    $tSambung->status = 'latest';
                    $tSambung->save();
                }

                if ($request->status_input[$i] == "deleted") {
                    $tSambung = DHopSupport::where('id', $request->id_tsambung[$i])->first();
                    $tSambung->status = 'deleted';
                    $tSambung->save();
                }
            }

          
            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }


    public function historyCore(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = DHopCoreHistory::where('id_hop', $request->idHope)
                ->with('getCore')
                ->with('getUpdatedBy')
                ->get();

            DB::commit();
            return response()->json([
                'status' => 'berhasil',
                'data' => $data
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updatePatroli(Request $request)
    {
        // dd($request->all());

        // dd($request->tipe, $request->keterangan, $request->lon, $request->lat );
        DB::beginTransaction();
        try {
            // dd($request->all());
            $now = Carbon::now()->format('Ymd-His');

            for ($i = 0; $i < count($request->status); $i++) {
                if ($request->status[$i] == "created") {
                    // dd($request->encode_foto[$i]);

                    // dd($request->id_hop_core[$i], $request->status_input[$i]);
                    // create ke table d_hop_core
                    $laporPatroli = new DLaporanPatroli();
                    $laporPatroli->id = DLaporanPatroli::max('id') + 1;
                    $laporPatroli->id_hop = $request->hop;
                    $laporPatroli->pelapor = $request->pelapor;
                    $laporPatroli->lon = $request->lon[$i];
                    $laporPatroli->lat = $request->lat[$i];
                    $laporPatroli->keterangan = $request->keterangan_pl[$i];
                    if ($request->encode_foto_pl[$i] != null) {
                        $foto_pl = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto_pl[$i]));
                        $laporPatroli->foto = $now . '-' . $laporPatroli->id . ".png";
                        Storage::disk('local')->put('/upload/laporanpatroli/hop/' . $laporPatroli->foto, $foto_pl);
                    }

                    $laporPatroli->status = 'latest';

                    $laporPatroli->save();
                }

                if ($request->status[$i] == "edited") {
                    // dd($request->all());

                    $laporPatroli = DLaporanPatroli::where('id', $request->id_laporan[$i])->first();

                    $laporPatroli->keterangan = $request->keterangan_pl[$i];
                    if ($request->encode_foto_pl[$i] != null) {
                        $foto_pl = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto_pl[$i]));
                        $laporPatroli->foto = $now . '-' . $laporPatroli->id . ".png";
                        Storage::disk('local')->put('/upload/laporanpatroli/hop/' . $laporPatroli->foto, $foto_pl);
                    }
                    $laporPatroli->status = 'latest';
                    $laporPatroli->save();
                }

                if ($request->status[$i] == "deleted") {
                    $laporPatroli = DLaporanPatroli::where('id', $request->id_laporan[$i])->first();
                    $laporPatroli->status = 'deleted';
                    $laporPatroli->save();
                }
            }

            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function simpanTSambung(Request $request)
    {
        DB::beginTransaction();
        try {
            $now = Carbon::now()->format('Ymd-His');
            
            $hop = DInventoryHop::where('id', $request->idHop)
                ->with('getLokasiAsal')
                ->with('getLokasiTujuan')
                ->first();

            $ruas = $hop->getLokasiAsal->abbreviation . '_' . $hop->getLokasiTujuan->abbreviation . '(' . $hop->jenis_kabel . ')-' . $hop->qrcode . '-' . $hop->nama_kabel;
            $watermarkText = Auth::user()->nama . "\n" . Carbon::createFromFormat('Ymd-His', $now)->isoFormat('D MMMM Y (HH:mm:ss)') . "\n" . $ruas . "\n" .  $request->lat_ts . ', ' . $request->lon_ts;
            $path = storage_path('app/upload/titiksambung/hop/');

            $tSambung = new DHopSupport();
            $tSambung->id_hop = $request->idHop;
            $tSambung->tipe = $request->tipe;
            $tSambung->lon = $request->lon_ts;
            $tSambung->lat = $request->lat_ts;
            $tSambung->updated_by = Auth::user()->id;
            $tSambung->keterangan = $request->keterangan;
            if ($request->encode_foto_tsambung != null) {
                $foto_ts = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto_tsambung));
                $imageWatermarked = ImageProcess::addWatermark($foto_ts, $watermarkText, 5, 57, 'string');
                $tSambung->foto = $now . '-' . $tSambung->id . ".png";
                imagepng($imageWatermarked, $path . $tSambung->foto);
            }
            if ($request->upload_ts != null) {
                $tSambung->tipe_laporan = 'blank spot';

                $imageResized = ImageProcess::resizeImageFromUpload($request->upload_ts, 480, 640);
                $imageWatermarked = ImageProcess::addWatermark($imageResized, $watermarkText, 5, 57, 'resize');
                $tSambung->foto = $now . '-' . $tSambung->id . ".png";
                imagepng($imageWatermarked, $path . $tSambung->foto);
            }
            $tSambung->status = 'latest';

            $tSambung->save();
        
            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);

        }
        
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }

    }
    
    public function editTSambung(Request $request)
    {

        // dd($request->all());
        $now = Carbon::now()->format('Ymd-His');

        DB::beginTransaction();
        try {

            $now = Carbon::now()->format('Ymd-His');
            
            $hop = DInventoryHop::where('id', $request->idHop)
                ->with('getLokasiAsal')
                ->with('getLokasiTujuan')
                ->first();

            $ruas = $hop->getLokasiAsal->abbreviation . '_' . $hop->getLokasiTujuan->abbreviation . '(' . $hop->jenis_kabel . ')-' . $hop->qrcode . '-' . $hop->nama_kabel;
            $watermarkText = Auth::user()->nama . "\n" . Carbon::createFromFormat('Ymd-His', $now)->isoFormat('D MMMM Y (HH:mm:ss)') . "\n" . $ruas . "\n" .  $request->lat_ts . ', ' . $request->lon_ts;
            $path = storage_path('app/upload/titiksambung/hop/');


            $tSambung = DHopSupport::where('id', $request->id_ts)->first();
            $tSambung->tipe = $request->tipe;
            $tSambung->lon = $request->lon_ts;
            $tSambung->lat = $request->lat_ts;
            $tSambung->keterangan = $request->keterangan;
            $tSambung->updated_by = Auth::user()->id;

            
            if ($request->encode_foto_tsambung != null) {
                $foto_ts = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto_tsambung));
                $tSambung->foto = $now . '-' . $tSambung->id . ".png";
                Storage::disk('local')->put('/upload/titiksambung/hop/' . $tSambung->foto, $foto_ts);
            }
            if ($request->upload_ts != null) {
                $tSambung->tipe_laporan = 'blank spot';

                $imageResized = ImageProcess::resizeImageFromUpload($request->upload_ts, 480, 640);
                $imageWatermarked = ImageProcess::addWatermark($imageResized, $watermarkText, 5, 57, 'resize');
                $tSambung->foto = $now . '-' . $tSambung->id . ".png";
                imagepng($imageWatermarked, $path . $tSambung->foto);
            }
            $tSambung->status = 'latest';

            $tSambung->save();
        
            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);

        }
        
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function hapusTSambung(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $hop = DHopSupport::findOrFail($request->id);
            $hop->status = 'deleted';
            $hop->update();
            
            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);
        
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateHop(Request $request)
    {
        DB::beginTransaction();
        try {
            // dd($request->all());
            $hop = DInventoryHop::where('id', $request->id)->first();
            
            $hop->nama_kabel = $request->nama_kabel;
            $hop->qrcode = $request->qrcode_hop;
            $hop->jenis_fo = $request->jenis_fo;
            $hop->jenis_kabel = $request->jenis_kabel;
            $hop->tipe = $request->tipemp;

            $hop->save();
        
            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);

        }
        
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }


    public function simpanLapPatroli(Request $request)
    {
        DB::beginTransaction();
        try {
            $now = Carbon::now()->format('Ymd-His');
            
            $cekArnet = DInventoryHop::where('id', $request->idHop)
                ->with('getLokasiAsal')
                ->with('getLokasiTujuan')
                ->first();

            $ruas = $cekArnet->getLokasiAsal->abbreviation . '_' . $cekArnet->getLokasiTujuan->abbreviation . '(' . $cekArnet->jenis_kabel . ')-' . $cekArnet->qrcode . '-' . $cekArnet->nama_kabel;
            $watermarkText = Auth::user()->nama . "\n" . Carbon::createFromFormat('Ymd-His', $now)->isoFormat('D MMMM Y (HH:mm:ss)') . "\n" . $ruas . "\n" .  $request->lat_lp . ', ' . $request->lon_lp;
            $path = storage_path('app/upload/laporanpatroli/hop/');

            $lapPatroli = new DLaporanPatroli();
            $lapPatroli->id = DLaporanPatroli::max('id') + 1;
            $lapPatroliId = $lapPatroli->id;
            $lapPatroli->id_hop = $request->idHop;
            $lapPatroli->pelapor = Auth::user()->id;
            $lapPatroli->lon = $request->lon_lp;
            $lapPatroli->lat = $request->lat_lp;
            $lapPatroli->eskalasi = $request->eskalasi;
            $lapPatroli->kategori_kegiatan = $request->kategori_kegiatan;
            $lapPatroli->kegiatan = $request->kegiatan;
            $lapPatroli->keterangan = $request->keterangan;
            if ($request->encode_foto_lap != null) {
                $foto_pl = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->encode_foto_lap));
                $imageWatermarked = ImageProcess::addWatermark($foto_pl, $watermarkText, 5, 57, 'string');
                $lapPatroli->foto = $now . '-' . $lapPatroli->id . ".png";
                imagepng($imageWatermarked, $path . $lapPatroli->foto);
            }
            if ($request->upload_foto != null) {
                $lapPatroli->tipe_laporan = 'blank spot';

                $imageResized = ImageProcess::resizeImageFromUpload($request->upload_foto, 480, 640);
                $imageWatermarked = ImageProcess::addWatermark($imageResized, $watermarkText, 5, 57, 'resize');
                $lapPatroli->foto = $now . '-' . $lapPatroli->id . ".png";
                imagepng($imageWatermarked, $path . $lapPatroli->foto);
            }
            $lapPatroli->status = 'latest';
            $lapPatroli->save();

            // 336,169,171,168,163,167,365,361  sesuai permintaan
            // if ($cekArnet->id_lokasi_asal == 336 || $cekArnet->id_lokasi_tujuan == 336 || $cekArnet->id_lokasi_asal == 169  || $cekArnet->id_lokasi_tujuan == 169 || $cekArnet->id_lokasi_asal == 171 || $cekArnet->id_lokasi_tujuan == 171 || $cekArnet->id_lokasi_asal == 168 || $cekArnet->id_lokasi_tujuan == 168 || $cekArnet->id_lokasi_asal == 163 || $cekArnet->id_lokasi_tujuan == 163 || $cekArnet->id_lokasi_asal == 167 || $cekArnet->id_lokasi_tujuan == 167 || $cekArnet->id_lokasi_asal == 365 || $cekArnet->id_lokasi_tujuan == 365 || $cekArnet->id_lokasi_asal == 361 || $cekArnet->id_lokasi_tujuan == 361) { // id sesuai permintaan
            //     $lapPatroli->lokasiAsal = $cekArnet->id_lokasi_asal;
            //     $lapPatroli->lokasiTujuan = $cekArnet->id_lokasi_tujuan;

            //     ScanQrController::kirimLapPatroliNtb($lapPatroli, $lapPatroliId);
            // }

            // if ($cekArnet->getLokasiAsal->parent == 9 || $cekArnet->getLokasiTujuan->parent == 9) { // id Arnet NTB = 9
            //     $lapPatroli->lokasiAsal = $cekArnet->getLokasiAsal->parent;
            //     $lapPatroli->lokasiTujuan = $cekArnet->getLokasiTujuan->parent;

            //     ScanQrController::kirimLapPatroliNtb($lapPatroli, $lapPatroliId);
            //     // dd('arnet NTB', $cekArnet, $cekArnet->getLokasiAsal->parent, $cekArnet->getLokasiTujuan->parent);
            // }
            // // dd('bukan NTB', $cekArnet, $cekArnet->getLokasiAsal->parent, $cekArnet->getLokasiTujuan->parent);

            if ($lapPatroli->eskalasi == "ya") {
                ScanQrController::kirimLapPatroli($lapPatroli, $lapPatroliId);
            }
        
            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);

        }
        
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }

    }

    public function kirimLapPatroli($lapPatroli, $lapPatroliId)
    {
        if ($lapPatroli != null) {
            $ruasData = DInventoryHop::where('id', $lapPatroli->id_hop)
                    ->with('getLokasiAsal')
                    ->with('getLokasiTujuan')
                    ->first();
            
            $ruas = $ruasData->getLokasiAsal->abbreviation . "_" . $ruasData->getLokasiTujuan->abbreviation . "(" . $ruasData->jenis_kabel . ")-" . $ruasData->qrcode . "-" . $ruasData->nama_kabel;

            $caption = "Laporan Patroli\n".
                    $ruas . "\n".
                    'Pelapor : ' . strtoupper(Auth::user()->nama) . "\n".
                    'Tanggal : ' . Carbon::parse($lapPatroli->created_at)->isoFormat('D MMMM Y') . "\n\n".
                    '<code>kategori   : ' . $lapPatroli->kategori_kegiatan . "</code>\n".
                    '<code>kegiatan   : ' . $lapPatroli->kegiatan . "</code>\n".
                    '<code>eskalasi   : ' . strtoupper($lapPatroli->eskalasi) . "</code>\n".
                    '<code>keterangan : ' . $lapPatroli->keterangan . "</code>\n\n".
                    '*) Laporan hanya muncul di grup telegram apabila eskalasi bernilai YA'."\n\n\n".
                    'lihat <a href="' . URL::to('/') . '/laporan/patroli/detail/' . $lapPatroliId . '">detail laporan</a>';
                    // 'lihat <a href="https://treg5.cloud/tcam/index.php/">detail laporan</a>';

            $response = Telegram::sendPhoto([
                // 'chat_id' => '963842330', // chat id ku
                'chat_id' => '-1001585716571', // chat id group fixom
                // 'photo' => 'https://wallpaperaccess.com/full/1397730.jpg',
                'photo' => str_replace('/index.php', '', URL::to('/')) . '/storage/app/upload/laporanpatroli/hop/' . $lapPatroli->foto,
                'parse_mode' => 'HTML',
                'caption' => $caption,
                'disable_web_page_preview' => false
            ]);
            $messageId = $response->getMessageId();
        }
    }

    // public function kirimLapPatroliNtb($lapPatroli, $lapPatroliId)
    // {
    //     if ($lapPatroli != null) {
    //         $ruasData = DInventoryHop::where('id', $lapPatroli->id_hop)
    //                 ->with('getLokasiAsal')
    //                 ->with('getLokasiTujuan')
    //                 ->first();
            
    //         $ruas = $ruasData->getLokasiAsal->abbreviation . "_" . $ruasData->getLokasiTujuan->abbreviation . "(" . $ruasData->jenis_kabel . ")-" . $ruasData->qrcode . "-" . $ruasData->nama_kabel;

    //         $caption = "Laporan Patroli\n".
    //                 $ruas . "\n".
    //                 'Pelapor : ' . strtoupper(Auth::user()->nama) . "\n".
    //                 'Tanggal : ' . Carbon::parse($lapPatroli->created_at)->isoFormat('D MMMM Y') . "\n\n".
    //                 '<code>kategori   : ' . $lapPatroli->kategori_kegiatan . "</code>\n".
    //                 '<code>kegiatan   : ' . $lapPatroli->kegiatan . "</code>\n".
    //                 '<code>eskalasi   : ' . strtoupper($lapPatroli->eskalasi) . "</code>\n".
    //                 '<code>keterangan : ' . $lapPatroli->keterangan . "</code>\n\n\n".
    //                 'lihat <a href="' . URL::to('/') . '/laporan/patroli/detail/' . $lapPatroliId . '">detail laporan</a>';
    //                 // 'lihat <a href="https://treg5.cloud/tcam/index.php/">detail laporan</a>';
            
    //         // if ($lapPatroli->lokasiAsal == 9 || $lapPatroli->lokasiTujuan == 9) {
    //         // if ($lapPatroli->lokasiAsal == 336 || $lapPatroli->lokasiTujuan == 336 || $lapPatroli->lokasiAsal == 169  || $lapPatroli->lokasiTujuan == 169 || $lapPatroli->lokasiAsal == 171 || $lapPatroli->lokasiTujuan == 171 || $lapPatroli->lokasiAsal == 168 || $lapPatroli->lokasiTujuan == 168 || $lapPatroli->lokasiAsal == 163 || $lapPatroli->lokasiTujuan == 163 || $lapPatroli->lokasiAsal == 167 || $lapPatroli->lokasiTujuan == 167 || $lapPatroli->lokasiAsal == 365 || $lapPatroli->lokasiTujuan == 365 || $lapPatroli->lokasiAsal == 361 || $lapPatroli->lokasiTujuan == 361) {
    //         Telegram::sendPhoto([
    //             'chat_id' => '-1001291913944', // chat id group NTB
    //             // 'chat_id' => '-1001585716571', // chat id group fixom
    //             'photo' => str_replace('/index.php', '', URL::to('/')) . '/storage/app/upload/laporanpatroli/hop/' . $lapPatroli->foto,
    //             'parse_mode' => 'HTML',
    //             'caption' => $caption,
    //             'disable_web_page_preview' => false
    //         ]);

    //         $response = Telegram::sendPhoto([
    //             'chat_id' => '-699014332', // chat id group MotoGP
    //             // 'chat_id' => '-1001585716571', // chat id group fixom
    //             'photo' => str_replace('/index.php', '', URL::to('/')) . '/storage/app/upload/laporanpatroli/hop/' . $lapPatroli->foto,
    //             'parse_mode' => 'HTML',
    //             'caption' => $caption,
    //             'disable_web_page_preview' => false
    //         ]);
    //         $messageId = $response->getMessageId();
    //         // }
    //     }
    // }

    public function hapusLapPatroli(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $lp = DLaporanPatroli::findOrFail($request->id);
            $lp->status = 'deleted';
            $lp->update();
            
            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);
        
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    // query insert masal
    public function insertMasalHopCore($start, $end)
    {
        // $start = 42;
        // $end = 60;
        set_time_limit(0);
        $idHop = array();

        for ($i=$start; $i <=$end ; $i++) { 
            array_push($idHop, $i);
        }

        $data = DInventoryHop::where('status', 'active')->whereIn('id', $idHop)->get();

        foreach ($data as $key => $value) {
            for ($i = 1; $i <= $value->jumlah_core; $i++) {
                // create ke table d_hop_core
                $hopCore = new DHopCore();
                $hopCore->id = DHopCore::max('id') + 1;
                $hopCore->id_hop = $value->id;
                // $hopCore->id_core = null;
                $hopCore->nama = 'Core_' . $i;
                // $hopCore->customer = null;
                // $hopCore->deskripsi = null;
                // $hopCore->hasil_ukur = null;
                // $hopCore->evidance = null;
                // $hopCore->tipe = null;
                $hopCore->status = 'latest';
                // $hopCore->updated_by = null;

                // create ke table d_hop_core_history
                $hopCoreHistory = new DHopCoreHistory();
                $hopCoreHistory->id = DHopCoreHistory::max('id') + 1;
                $hopCoreHistory->id_hop = $hopCore->id_hop;
                // $hopCoreHistory->id_core = $hopCore->id_core;
                $hopCoreHistory->nama = $hopCore->nama;
                // $hopCoreHistory->customer = $hopCore->customer;
                // $hopCoreHistory->deskripsi = $hopCore->deskripsi;
                // $hopCoreHistory->hasil_ukur = $hopCore->hasil_ukur;
                // $hopCoreHistory->evidance = $hopCore->evidance;
                // $hopCoreHistory->tipe = $hopCore->tipe;
                $hopCoreHistory->status = 'created';
                // $hopCoreHistory->updated_by = $hopCore->updated_by;

                // save
                $hopCoreHistory->save();
                $hopCore->save();
            }
        }
    }

    public function updateMasalHopCore($start, $end)
    {
        // $start = 42;
        // $end = 60;

        set_time_limit(0);
        $idHop = array();

        for ($i=$start; $i <=$end ; $i++) { 
            array_push($idHop, $i);
        }

        $data = DInventoryHop::where('status', 'active')->whereIn('id', $idHop)->get();
        
        foreach ($data as $key => $value) {            
            for ($i = 0; $i < $value->jumlah_core; $i++) {
                // create ke table d_hop_core               
                $hopCore = DHopCore::where('id_hop', $value->id)->get();
                
                foreach ($hopCore as $key => $hopCoreValue) {
                    $nama = $key + 1;

                    if($hopCoreValue->nama == NULL || $hopCoreValue->nama == 'null'){
                        $hopCoreValue->nama = 'Core_'.$nama;

                        $hopCoreValue->save();
                    }
       
                }
            }
        }
    }
}
