<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Models\MLocationFixom;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Storage;
use URL;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function isLogin()
    {
        if (!Auth::user()) {
            return redirect()->route('login');
        }
    }

    public function loginCek(Request $request)
    {

        DB::beginTransaction();
        try {
            if (Auth::attempt($request->only('email', 'password'))) {
                if (auth()->user()->status == 'active') {
                    return response()->json([
                        'status'    => 'berhasil',
                        'message'   => 'Login Berhasil'
                    ]);
                } elseif (auth()->user()->status == 'pending') {
                    Auth::logout();
                    return response()->json([
                        'status'    => 'pending',
                        // 'message'   => 'Akun anda belum aktif, silahkan hubungi developer melalui telegram @hanifkuncahyo'
                        'message'   => 'Akun anda belum aktif, silahkan konfirmasi di grup Zero FO-Cut'

                    ]);
                } elseif (auth()->user()->status == 'rejected') {
                    Auth::logout();
                    return response()->json([
                        'status'    => 'rejected',
                        // 'message'   => 'Akun anda ditolak / tidak disetujui, silahkan hubungi developer melalui telegram @hanifkuncahyo'
                        'message'   => 'Akun anda ditolak / tidak disetujui, silahkan konfirmasi di grup Zero FO-Cut'

                    ]);
                }
            }

            return response()->json([
                'status'    => 'gagal',
                'message'   => 'Email atau password yang anda masukkan salah'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function register()
    {
        $arnet = MLocationFixom::where('status', 'active')
                ->where('level', 'WITEL')
                ->get();

        return view('auth.register', compact('arnet'));
    }

    public function simpan(Request $request)
    {
        DB::beginTransaction();
        try {
            $cek_email = User::where('email', $request->email)->first();
            $cek_nik = User::where('nik', $request->nik)->first();

            if ($cek_email == null && $cek_nik == null) {
                $user = new User();
                $user->id = User::max('id') + 1;
                $user->role = $request->role;
                $loker = join(",", $request->arnet);
                $user->loker = "," . $loker . ",";
                $user->nik = $request->nik;
                $user->nama = $request->nama;
                $user->email = $request->email;
                $user->no_hp = $request->no_hp;
                $user->password = bcrypt($request->password);
                $user->status = 'accepted';
                $user->save();

                DB::commit();
                return response()->json([
                    'status' => 'berhasil'
                ]);
            } else if ($cek_email != null) {
                return response()->json([
                    'status' => 'ada',
                    'message' => 'Email sudah ada, silahkan ganti dengan email yang lain'
                ]);
            } else if ($cek_nik != null) {
                return response()->json([
                    'status' => 'ada',
                    'message' => 'NIK sudah ada, silahkan ganti dengan nik yang lain'
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login')->with('berhasil', 'Logout berhasil');
    }

    public function kirimEmail(Request $request)
    {
        DB::beginTransaction();
        try {
            $url = URL::to('/') . '/reset-password'; 
            $data = User::where('nik', $request->nik)->first();

            if ($data != null) {
                $data->link = $url . '/' . $data->id;
                Mail::to($data->email)->send(new SendMail($data->nama, $data->link));

                return response()->json([
                    'status' => 'ada'
                ]);

            } else {
                return response()->json([
                    'status' => 'kosong'
                ]);
            }
        
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }        
    }

    public function resetPassword($id)
    {
        return view('auth.reset-password', compact('id'));
    }

    public function simpanPassword(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($request->id_user);
            $user->password = bcrypt($request->password);
            $user->update();

            DB::commit();  
            return response()->json([
                'status' => 'berhasil'
            ]);
        
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }
}
