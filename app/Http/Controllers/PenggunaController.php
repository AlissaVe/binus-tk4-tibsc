<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    public function index(Request $request)
    {
        $data = User::all();
        return view('pengguna.index', compact('data'));
    }

    public function tambah()
    {
        return view('pengguna.tambah');
    }

    public function hapus($id)
    {
        User::where('user_id', $id)->delete();
        return redirect()->route('pengguna.index')->with('success', 'Pengguna Deleted successfully!');
    }

    function tambah2(Request $request){


        $dt = new User();
        $dt->username = $request->username;
        $dt->password = Hash::make($request->password);
        $dt->hak_akses = $request->hak_akses;
        $dt->save();
    
        // $dt->pertanyaan = $request->pertanyaan;
        // $dt->id_dimensi = $request->id_dimensi;
        // $dt->variabel = $request->variabel;
        // $dt->pila = $request->pila;
        // $dt->pilb = $request->pilb;
        // $dt->pilc = $request->pilc;
        // $dt->pild = $request->pild;
        // $dt->pile = $request->pile;
        // $dt->save();
        return redirect()->route('pengguna.tambah')->with('success', 'Pengguna Added successfully!');

    }
}
