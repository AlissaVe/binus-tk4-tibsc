<?php

namespace App\Http\Controllers;

use App\Models\DHopCore;
use App\Models\DHopCoreHistory;
use App\Models\DHopSupport;
use App\Models\DInventoryHop;
use App\Models\DLaporanPatroli;
use App\Models\MCore;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Storage;

class WebcamTest extends Controller
{
    public function index(Request $request)
    {
        $core = MCore::where('status', 'active')->get();
        
        return view('webcamtest.webcam', compact('core'));
    }
}