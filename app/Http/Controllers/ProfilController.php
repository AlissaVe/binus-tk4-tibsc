<?php

namespace App\Http\Controllers;

use App\Models\MLocation;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    public function index(Request $request)
    {
        $arnet = MLocation::where('status', 'active')->where('level', 'WITEL')->orderBy('fullname', 'ASC')->get();

        $data = User::where('id', Auth::user()->id)->first();
        $loker = substr($data->loker, 1, -1);
        $loker = explode(',', $loker);
        $loker = MLocation::whereIn('id', $loker)->get();
        $data->loker = $loker;
        
        return view('profil.index', compact('arnet', 'data'));
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = User::where('id', Auth::user()->id)->first();
            $loker = join(",", $request->arnet);
            $user->loker = "," . $loker . ",";
            $user->nama = $request->nama;
            $user->no_hp = $request->nohp;
            if ($request->password != null) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            DB::commit();
            return response()->json([
                'status' => 'berhasil'
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'gagal',
                'message' => $e->getMessage()
            ]);
        }
    }
}
