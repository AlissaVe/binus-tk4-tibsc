<?php

namespace App\Http\Controllers;

use App\Models\DPertanyaan;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function index(Request $request)
    {
        $data = DPertanyaan::all();
        return view('pertanyaan.index', compact('data'));
    }

    public function tambah()
    {
        return view('pertanyaan.tambah');
    }

    public function hapus($id)
    {
        DPertanyaan::where('id_kuesioner', $id)->delete();
        return redirect()->route('pertanyaan.index')->with('success', 'Pertanyaan Deleted successfully!');
    }

    function tambah2(Request $request){
        $dt = new DPertanyaan();
        $dt->pertanyaan = $request->pertanyaan;
        $dt->id_dimensi = $request->id_dimensi;
        $dt->variabel = $request->variabel;
        $dt->pila = $request->pila;
        $dt->pilb = $request->pilb;
        $dt->pilc = $request->pilc;
        $dt->pild = $request->pild;
        $dt->pile = $request->pile;
        $dt->save();
        return redirect()->route('pertanyaan.tambah')->with('success', 'Pertanyaan Added successfully!');

    }
}
