<?php

namespace App\Helpers;

use DB;

class ImageProcess
{
    // parameter file gambar, witdh, height harus diisi
    public static function resizeImageFromUpload($file, $newWidth, $newHeight)
    {
        // info file gambar
        $info = getimagesize($file);
        // ambil ekstensi file gambar
        $mime = $info['mime'];
    
        // cek ekstensi file gambar
        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default: 
                throw new Exception('Unknown image type.');
        }

        // image create sesuai ekstensi
        $img = $image_create_func($file);
        // ambil width dan height gambar
        list($width, $height) = getimagesize($file);

        // $newHeight = ($height / $width) * $newWidth;

        // temp resize gambar
        $tmp = imagecreatetruecolor($newWidth, $newHeight);
        // resize gambar
        imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        // dd($tmp);
        return $tmp;
    }

    // parameter image, watermark, posisi horizontal, posisi vertikal, category(pilih salah satu) = 'string' / 'file' / 'resize'(dari resize image)
    // parameter harus diisi
    public static function addWatermark($image, $watermark, $positionX, $positionY, $category)
    {
        // cek category
        if ($category == 'string') {
            $imagetobewatermark = imagecreatefromstring($image);
        } else {
            $imagetobewatermark = $image;
        }

        $font = public_path('font/century-gothic.ttf');
        $fontsize = "9";
        // posisi horizontal watermark
        $x = $positionX;
        // posisi vertikal watermark
        $y = $positionY; 
        // ambil width gambar
        $imgx = imagesx($imagetobewatermark);
        // ambil height gambar 
        $imgy = imagesy($imagetobewatermark);
        // warna watermark 
        $white = imagecolorallocate($imagetobewatermark, 255, 255, 255);
        // gambar diberi watermark
        imagettftext($imagetobewatermark, $fontsize, 0, $x, $imgy - $y, $white, $font, $watermark);
        header("Content-type:image/png");

        return $imagetobewatermark;
    }
}
